//---- jQuery ----

//= '../../node_modules/jquery/dist/jquery.min.js'

//---- OWL ----

//= '../../node_modules/owl-carousel/owl-carousel/owl.carousel.min.js'

//---- Validate ----

//= '../../node_modules/jquery-validation/dist/jquery.validate.min.js'
//= '../../node_modules/jquery-validation/dist/localization/messages_ru.min.js'

//---- Inputmask ----

//= '../../node_modules/inputmask/dist/jquery.inputmask.min.js'

//---- ymaps-touch-scroll ----

//= '../../node_modules/ymaps-touch-scroll/dist/ymaps-touch-scroll.js'